package kot.lk.vinu;

import java.io.*;
import java.util.Scanner;

public class KOT {
	public void startEat() {

	//public static void main(String args[]) throws IOException {
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// user input
	 Scanner scan=new Scanner(System.in);
		// pass menu orders
		String orderNameM[] = { " ", "fish curry", "Chicken rice", "Noodle", "Kottu", "Ginataan", "CamoteCue",
				"Kalamay", "NataDeCoco", "Coke", "Sprite", "Pepsi", "7-up" };
		// Display food price
		String orderNameP[] = { " ", "fish curry\tRs 300.00", "Chicken rice\tRs 420.00", "Noodle\tRs 250.00",
				"Kottu\tRs 250.00", "Ginataan\t\tRs 25.00", "CamoteCue\t\tRs 20.00", "Kalamay\t\tRs 25.00",
				"Nata de Coco\tRs 20.00", "Coke\t\t\tRs150.00", "Sprite\t\t\tRs 130.00", "Pepsi\t\t\tRs 125.00",
				"7-up\t\t\tRs 125.00" };

		// pass food price
		double orderPrice[] = { 0.00, 300.00, 420.00, 250.00, 250.00, 25.00, 20.00, 25.00, 20.00, 150.0, 130.00, 125.00,
				125.00 };
		// variable declaration  
		String user, pass, search, again = "", mainQ = "", mainQ2 = "", dessertQ = "", dessertQ2 = "", drinksQ = "",
				drinksQ2 = "";
       
		int a = 0, b = 0, c = 0, d = 0, e = 1, g = 0, h = 0, i = 0, r = 0, choice = 0, choice1 = 0, order = 0,
				table = 1, end = 0;
		//Store order in 2D array 20 is a number of maximum order 
		String orderString[][] = new String[50][20];//50 is the table number
		String orderStringM[][] = new String[0][20];
		String orderStringP[][] = new String[20][20];
		double orderMDouble[][] = new double[50][20];
		double orderDDouble[][] = new double[50][20];
		double tableNo[] = new double[21];
		double noOfItem[] = new double[13];
		double paid[] = new double[21];
		double totalOrder[] = new double[50];
		double payment, change = 0;
		double totalPrice[] = new double[21];
		double orderDouble[][] = new double[21][20];

		for (int z = 1; z <= 12; z++) {
			noOfItem[z] = 20;
		}

		while (a < 3) {
			System.out.print("\nEnter Username: ");
			user = scan.nextLine();
			System.out.print("\nEnter Password: ");
			pass = scan.nextLine();

			if (user.equalsIgnoreCase("vinu") && pass.equalsIgnoreCase("123")) {

				System.out.println("\n ******************************************************");
				System.out.println(" ********** Welcome To jaffna Kitchen ordering System **********");
				System.out.println(" *******************************************************");

				do {
					System.out.println("\n **************************************");
					System.out.println(" ********** M A I N  M E N U **********");
					System.out.println(" **************************************");
					System.out.println(" (1) Order");
					System.out.println(" (2) Order Information");
					System.out.println(" (3) Billing");
					System.out.println(" (4) Dish Inventory");
					System.out.println(" (5) Exit");
					System.out.println(" *************************************");

					for (int f = 1; f == 1;) {

						System.out.print("\nEnter Choice: ");
						choice = Integer.parseInt(scan.nextLine());

						// CHOICE 1 - "ORDER"

						if (choice == 1) {
							do {
								for (int z = 1; z <= 12; z++) {
									orderString[b][z] = "0";
								}
								for (int x = 2; x == 2;) {
									System.out.print("\nEnter Customer Name: ");
									orderString[b][0] = scan.nextLine();

									x = 0;

									for (int l = 0; l < b; l++) {
										if (orderString[l][0].equalsIgnoreCase(orderString[b][0])) {
											System.out.println("Customer Name Already Used!");
											x = 2;
										}
									}
								}

								c = 0;
								System.out.println("\n *************************************");
								System.out.println(" ************* DISH MENU ************* ");
								System.out.println(" *************************************");
								System.out.println("\n    ********** MAIN DISH **********");
								System.out.print(" fish curry \t\tRS 300.00");
								if (noOfItem[1] > 0) {
									System.out.println("\t" + noOfItem[1]);
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Chicken rice \t\tRs 420.00");
								if (noOfItem[2] > 0) {
									System.out.println("\t" + noOfItem[2]);
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Noodle \t\tRs 250.00");
								if (noOfItem[3] > 0) {
									System.out.println("\t" + noOfItem[3]);
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Kotttu\t\t Rs250.00");
								if (noOfItem[4] > 0) {
									System.out.println("\t" + noOfItem[4] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.println("\n    *********** DESSERT ***********");
								System.out.print(" Ginataan \t\t\tRs 25.00");
								if (noOfItem[5] > 0) {
									System.out.println("\t" + noOfItem[5] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" CamoteCue \t\t\tRs  20.00");
								if (noOfItem[6] > 0) {
									System.out.println("\t" + noOfItem[6] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Kalamay \t\t\tRs  25.00");
								if (noOfItem[7] > 0) {
									System.out.println("\t" + noOfItem[7] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Nata de Coco \t\tRs  20.00");
								if (noOfItem[8] > 0) {
									System.out.println("\t" + noOfItem[8] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.println("\n    *********** DRINKS ************");
								System.out.print(" Coke \t\t\t\tRs  150.00");
								if (noOfItem[9] > 0) {
									System.out.println("\t" + noOfItem[9] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Sprite \t\t\tRs  130.00");
								if (noOfItem[10] > 0) {
									System.out.println("\t" + noOfItem[10] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Pepsi \t\t\t\tRs  125.00");
								if (noOfItem[11] > 0) {
									System.out.println("\t" + noOfItem[11] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" 7-Up \t\t\t\tRs  125.00");
								if (noOfItem[12] > 0) {
									System.out.println("\t" + noOfItem[12] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.println(" *************************************");

								if (noOfItem[1] == 0 && noOfItem[2] == 0 && noOfItem[3] == 0 && noOfItem[4] == 0) {
									System.out.println("\nMain Dish Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to order MAIN DISH? [Y/N]: ");
										mainQ = scan.nextLine();

										// MAIN DISH

										if (mainQ.equalsIgnoreCase("y")) {

											do {
												System.out.println("\n\t ********** MAIN DISH **********");
												System.out.println(" **************************************");
												System.out.println(" NAME\t\t\t\tPRICE");
												System.out.print(" 1. fish rice " + "\tRs  300.00");
												if (noOfItem[1] > 0) {
													System.out.println("\t" + noOfItem[1] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 2. Chicken rice " + "\tPhp 420.00");
												if (noOfItem[2] > 0) {
													System.out.println("\t" + noOfItem[2] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 3. Noodle " + "\tRs 250.00");
												if (noOfItem[3] > 0) {
													System.out.println("\t" + noOfItem[3] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 4. Kottu " + "\tRs 250.00");
												if (noOfItem[4] > 0) {
													System.out.println("\t" + noOfItem[4] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.println(" **************************************");

												for (e = 1; e == 1;) {
													System.out.print("\nEnter Your Order: ");
													order = Integer.parseInt(scan.nextLine());

													for (v = 1; v <= 4; v++) {
														if (order == v) {
															if (orderString[b][v].equals("1")) {
																System.out.println("Dish Already Listed!");
																e = 1;
															} else if (noOfItem[v] == 0) {
																System.out.println("Dish Not Available");
																e = 1;
															} else {
																e = 0;
															}
														}
													}
													if (order < 1 || order > 4) {
														System.out.println("Invalid Input!");
														e = 1;
													}

												}

												do {
													System.out.print("How many? : ");
													orderMDouble[b][c] = Double.parseDouble(scan.nextLine());//convert string to Int

													for (v = 1; v <= 4; v++) {
														if (order == v) {
															orderString[b][v] = "1";
															noOfItem[v] = noOfItem[v] - orderMDouble[b][c];
															if (noOfItem[v] < 0) {
																noOfItem[v] = noOfItem[v] + orderMDouble[b][c];
																System.out.println("Sorry, We only have " + noOfItem[v]
																		+ "pcs. Available");
																v = 5;
																r = 1;
															} else {
																r = 0;
															}
														}
													}
												} while (r == 1);

												orderStringP[b][c] = orderNameP[order];
												orderStringM[b][c] = orderNameM[order];
												orderDouble[b][c] = orderPrice[order];

												c++;

												for (d = 1; d == 1;) {
													System.out.print("\nWant to Order Other MAIN DISH? [Y/N]: ");
													mainQ2 = scan.nextLine();

													if (mainQ2.equalsIgnoreCase("y")) {
														d = 0;
													} else if (mainQ2.equalsIgnoreCase("n")) {
														System.out.print("");
														d = 0;
													} else {
														System.out.print("Invalid Input!");
														d = 1;
													}
												}
												if (orderString[b][1].equals("1") && orderString[b][2].equals("1")
														&& orderString[b][3].equals("1")
														&& orderString[b][4].equals("1")) {
													System.out.println("\nSorry, You order all 4 MAIN DISH!");
													mainQ2 = "n";
												}
												if (noOfItem[1] == 0 && noOfItem[2] == 0 && noOfItem[3] == 0
														&& noOfItem[4] == 0) {
													System.out.println("\nMain Dish Not Available!");
													mainQ2 = "n";
												}
											} while (mainQ2.equalsIgnoreCase("y"));

										} else if (mainQ.equalsIgnoreCase("n")) {
											v = 0;
										} else {
											System.out.print("Invalid Input!");
											v = 1;
										}
									}
								}
								if (noOfItem[5] == 0 && noOfItem[6] == 0 && noOfItem[7] == 0 && noOfItem[8] == 0) {
									System.out.println("\nMain Dish Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to order DESSERT? [Y/N]: ");
										dessertQ = scan.nextLine();

										// DESSERT

										if (dessertQ.equalsIgnoreCase("y")) {
											do {
												System.out.println("\n\t ********** DESSERT **********");
												System.out.println(" **************************************");
												System.out.println(" NAME\t\t\t\tPRICE");
												System.out.print(" 1. Ginataan \t\tRs 25.00");
												if (noOfItem[5] > 0) {
													System.out.println("\t" + noOfItem[5] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 2. CamoteCue \t\tRs 20.00");
												if (noOfItem[6] > 0) {
													System.out.println("\t" + noOfItem[6] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 3. Kalamay \t\tRs 25.00");
												if (noOfItem[7] > 0) {
													System.out.println("\t" + noOfItem[7] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 4. Nata de Coco \tRs 20.00");
												if (noOfItem[8] > 0) {
													System.out.println("\t" + noOfItem[8] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.println(" **************************************");

												for (e = 1; e == 1;) {
													System.out.print("\nEnter Your Order: ");
													order = Integer.parseInt(scan.nextLine());
													order = order + 4;

													for (v = 5; v <= 8; v++) {
														if (order == v) {
															if (orderString[b][v].equals("1")) {
																System.out.println("Dessert Already Listed!");
																e = 1;
															} else if (noOfItem[v] == 0) {
																System.out.println("Dessert Not Available");
																e = 1;
															} else {
																e = 0;
															}
														}
													}
													if (order < 5 || order > 8) {
														System.out.println("Invalid Input!");
														e = 1;
													}
												}

												do {
													System.out.print("How many? : ");
													orderMDouble[b][c] = Double.parseDouble(scan.nextLine());

													for (v = 5; v <= 8; v++) {
														if (order == v) {
															orderString[b][v] = "1";
															noOfItem[v] = noOfItem[v] - orderMDouble[b][c];
															if (noOfItem[v] < 0) {
																noOfItem[v] = noOfItem[v] + orderMDouble[b][c];
																System.out.println("Sorry, We only have " + noOfItem[v]
																		+ "pcs. Available");
																v = 9;
																r = 1;

															} else {
																r = 0;
															}
														}

													}
												} while (r == 1);

												orderStringP[b][c] = orderNameP[order];
												orderStringM[b][c] = orderNameM[order];
												orderDouble[b][c] = orderPrice[order];

												c++;

												for (d = 1; d == 1;) {
													System.out.print("\nWant to Order Other DESSERT? [Y/N]: ");
													dessertQ2 = scan.nextLine();

													if (dessertQ2.equalsIgnoreCase("y")) {
														d = 0;
													} else if (dessertQ2.equalsIgnoreCase("n")) {
														d = 0;
														dessertQ = "n";
													} else {
														System.out.print("Invalid Input!");
														d = 1;
													}
												}
												if (orderString[b][5].equals("1") && orderString[b][6].equals("1")
														&& orderString[b][7].equals("1")
														&& orderString[b][8].equals("1")) {
													System.out.print("Sorry, You Order All 4 DESSERT!");
													dessertQ2 = "n";
												}
												if (noOfItem[1] == 0 && noOfItem[2] == 0 && noOfItem[3] == 0
														&& noOfItem[4] == 0) {
													System.out.println("\nMain Dish Not Available!");
													dessertQ2 = "n";
												}
											} while (dessertQ2.equalsIgnoreCase("y"));

										} else if (dessertQ.equalsIgnoreCase("n")) {
											v = 0;
										} else {
											System.out.print("Invalid Input!");
											v = 1;
										}
									}
								}
								if (noOfItem[5] == 0 && noOfItem[6] == 0 && noOfItem[7] == 0 && noOfItem[8] == 0) {
									System.out.println("\nDessert Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to Order DRINKS? [Y/N]: ");
										drinksQ = scan.nextLine();

										// DRINKS
										// to avoid case sensitives
										if (drinksQ.equalsIgnoreCase("y")) {

											do {
												System.out.println("\n\t ********** DRINKS **********");
												System.out.println(" **************************************");
												System.out.println(" NAME\t\t\t\tPRICE");
												System.out.print(" 1. Coke \t\t\tRs 150.00");
												if (noOfItem[9] > 0) {
													System.out.println("\t" + noOfItem[9] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 2. Sprite \t\t\tRs 130.00");
												if (noOfItem[10] > 0) {
													System.out.println("\t" + noOfItem[10] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 3. Pepsi \t\t\tRs 125.00");
												if (noOfItem[11] > 0) {
													System.out.println("\t" + noOfItem[11] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 4. 7-Up \t\t\tRs 125.00");
												if (noOfItem[12] > 0) {
													System.out.println("\t" + noOfItem[12] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.println(" **************************************");

												for (e = 1; e == 1;) {
													System.out.print("\nEnter Your Order: ");
													order = Integer.parseInt(scan.nextLine());
													order = order + 8;

													for (v = 9; v <= 12; v++) {
														if (order == v) {
															if (orderString[b][v].equals("1")) {
																System.out.println("Drinks Already Listed!");
																e = 1;
															} else if (noOfItem[v] == 0) {
																System.out.println("Drinks Not Available");
																e = 1;
															} else {
																e = 0;
															}
														}
													}
													if (order < 9 || order > 12) {
														System.out.println("Invalid Input!");
														e = 1;
													}
												}

												orderStringP[b][c] = orderNameP[order];
												orderStringM[b][c] = orderNameM[order];
												orderDouble[b][c] = orderPrice[order];

												do {

													System.out.print("How many? : ");
													orderMDouble[b][c] = Double.parseDouble(scan.nextLine());

													for (v = 9; v <= 12; v++) {
														if (order == v) {
															orderString[b][v] = "1";
															noOfItem[v] = noOfItem[v] - orderMDouble[b][c];
															if (noOfItem[v] < 0) {
																noOfItem[v] = noOfItem[v] + orderMDouble[b][c];
																System.out.println("Sorry, We only have " + noOfItem[v]
																		+ "pcs. Available");
																v = 13;
																r = 1;
															} else {
																r = 0;
															}
														}
													}
												} while (r == 1);

												c++;

												for (d = 1; d == 1;) {
													System.out.print("\nWant to Order Other DRINKS? [Y/N]: ");
													drinksQ2 = scan.nextLine();

													if (drinksQ2.equalsIgnoreCase("y")) {
														d = 0;
														c++;
													} else if (drinksQ2.equalsIgnoreCase("n")) {
														System.out.print("");
														d = 0;
														f = 0;
													} else {
														System.out.print("Invalid Input!");
														d = 1;
													}
												}
												if (orderString[b][9].equals("1") && orderString[b][10].equals("1")
														&& orderString[b][11].equals("1")
														&& orderString[b][12].equals("1")) {
													System.out.print("Sorry, You order all 4 DRINKS!");
													drinksQ2 = "n";
													f = 0;
												}
												if (noOfItem[9] == 0 && noOfItem[10] == 0 && noOfItem[11] == 0
														&& noOfItem[12] == 0) {
													System.out.println("\nDrinks Not Available!");
													drinksQ2 = "n";
													f = 0;
												}
											} while (drinksQ2.equalsIgnoreCase("y"));

										} else if (drinksQ.equalsIgnoreCase("n")) {
											v = 0;
											f = 0;
										} else {
											System.out.print("Invalid Input!");
											v = 1;
										}
									}
								}
								if (c == 0) {
									System.out.println("You Don't Have Any Order!");
									r = 1;
								} else {
									c--;
								}
							} while (r == 1);
							tableNo[b] = table;
							totalOrder[b] = c;
							paid[b] = 0;

							System.out.println("\n YOU'RE TABLE NO. IS: " + tableNo[b]);
							System.out.println(" YOU'RE ORDER ARE: ");
							for (int y = 0; y <= totalOrder[b]; y++) {
								System.out.println("   " + orderMDouble[b][y] + " pcs.\t" + orderStringM[b][y]);
								totalPrice[b] = totalPrice[b] + (orderDouble[b][y] * orderMDouble[b][y]);
							}

							table++;
							b++;
						}

						// CHOICE 2 - "ORDER INFO"

						else if (choice == 2) {

							do {

								System.out.print("\nEnter Customer Name: ");
								search =scan.nextLine();

								int s = 1;

								for (int x = 0; x < b; x++) {

									if (search.equalsIgnoreCase(orderString[x][0])) {
										System.out.println(" ******   CUSTOMER ORDER INFO   *******");
										System.out.println(" Customer Name: " + orderString[x][0]);
										System.out.println(" Table Number: " + tableNo[x]);
										System.out.println(" Customer Order:");

										for (int y = 0; y <= totalOrder[x]; y++) {
											System.out.println(
													"   " + orderMDouble[x][y] + " Pcs\t" + orderStringP[x][y]);
										}
										System.out.println(" --------------------------------------");
										System.out.print(" Total Bill: Php" + totalPrice[x]);
										if (paid[x] == 1) {
											System.out.println(" *PAID*");
											System.out.println(" **************************************");
										} else {
											System.out.println(" *NOT PAID*");
											System.out.println(" **************************************");
											System.out.println("\n Please Proceed To Billing!");
										}
										s = 0;
									}

								}

								if (s == 1) {
									System.out.println("Customer Name not found!");
									g = 1;
								} else {
									g = 0;
								}

								f = 0;
							} while (g == 1);

						}

						else if (choice == 3) {

							do {

								System.out.print("\nEnter Customer Name: ");
								search = scan.nextLine();

								int s = 1;
								for (int x = 0; x < b; x++) {
									if (search.equalsIgnoreCase(orderString[x][0])) {
										System.out.println(" ****   CUSTOMER ORDER RECEIPT   ****");
										System.out.println(" Customer Name: " + orderString[x][0]);
										System.out.println(" Table Number: " + tableNo[x]);
										System.out.println(" Customer Order:");

										for (int y = 0; y <= totalOrder[x]; y++) {
											System.out.println(
													"   " + orderMDouble[x][y] + " Pcs\t" + orderStringP[x][y]);
										}
										System.out.println(" **************************************");
										System.out.print(" Total Bill: Php " + totalPrice[x]);
										if (paid[x] == 1) {
											System.out.println(" *PAID*");
											System.out.println(" **************************************");
										} else {
											System.out.println("\n **************************************");
											for (int m = 1; m == 1;) {
												System.out.print("Enter Payment: ");
												payment = Double.parseDouble(scan.nextLine());

												change = payment - totalPrice[x];
												if (change < 0) {
													m = 1;
													System.out
															.println("Insufficient Amount of Money! Please Try Again!");
												} else {
													paid[x] = 1;
													m = 0;
												}
											}
											System.out.println("Change: Php" + change);
										}

										s = 0;
									}
								}
								if (s == 1) {
									System.out.println("Customer Name not found!");
									h = 1;
								} else {
									h = 0;
								}
								f = 0;

							} while (h == 1);
						} else if (choice == 4) {
							System.out.println(" ********** DISH  INVENTORY **********");
							System.out.println(" **************************************");
							System.out.println("\n   *********** MAIN  DISH ***********");
							System.out.println("   **********************************");
							for (int z = 1; z <= 4; z++) {
								System.out.println(" \t" + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println("   ************ DESSERT *************");
							System.out.println("   **********************************");
							for (int z = 5; z <= 8; z++) {
								System.out.println(" \t" + noOfItem[z] + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println("   ************* DRINKS *************");
							System.out.println("   **********************************");
							for (int z = 9; z <= 12; z++) {
								System.out.println(" \t" + noOfItem[z] + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println(" **************************************");
							f = 0;
						} else if (choice == 5) {
							f = 0;
							end = 1;
							again = "n";

							System.out.println("Thank You and Come Again!!");

						} else {
							System.out.println("Invalid Input!");
							f = 1;
						}

					}
					if (end == 0) {
						do {

							System.out.print("\nDo you want Another Transaction [Y/N]?");
							again = scan.nextLine();

							if (again.equalsIgnoreCase("n")) {
								i = 0;
							} else if (again.equalsIgnoreCase("y")) {
								i = 0;
							} else {
								System.out.println("Invalid Input!");
								i = 1;
							}
						} while (i == 1);
					}
				} while (again.equalsIgnoreCase("y"));

				break;
			} else {
				System.out.println("Invalid User or Password!!");
				a++;
			}
		}

	}

}
